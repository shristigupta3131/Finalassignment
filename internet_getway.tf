resource "aws_internet_gateway" "ninja_igw_01" {
  vpc_id = aws_vpc.ninja_vpc_01.id

  tags = {
    Name = "internet-gw"
  }
}
