resource "aws_route_table" "ninja_route_pub_01" {
  vpc_id = aws_vpc.ninja_vpc_01.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ninja_igw_01.id
  }
}

resource "aws_route_table_association" "public-rta" {
  subnet_id      = aws_subnet.ninja_pub_sub_01.id
  route_table_id = aws_route_table.ninja_route_priv_01.id
}
