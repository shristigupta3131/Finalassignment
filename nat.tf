resource "aws_eip" "ninja_nat_01" {
  vpc = true
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.ninja_nat_01.id
  subnet_id     = aws_subnet.ninja_pub_sub_01.id
  depends_on    = [aws_internet_gateway.ninja_igw_01]
}

resource "aws_route_table" "ninja_route_priv_01" {
  vpc_id = aws_vpc.ninja_vpc_01.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-gw.id
  }
}

resource "aws_route_table_association" "private-rta" {
  subnet_id      = aws_subnet.ninja_priv_sub_01.id
  route_table_id = aws_route_table.ninja_route_priv_01.id
}

