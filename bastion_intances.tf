resource "aws_instance" "bastion-host" {
  ami           = "ami-0c02fb55956c7d316"
  instance_type = "t2.micro"

  subnet_id = aws_subnet.ninja_pub_sub_01.id

  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  key_name = aws_key_pair.mykeypair.key_name

  tags = {
    Name = "bastion-instance"
  }
}
resource "aws_instance" "private_host" {
  ami           = "ami-0c02fb55956c7d316"
  instance_type = "t2.micro"

  subnet_id = aws_subnet.ninja_priv_sub_01.id

  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  key_name = aws_key_pair.mykeypair.key_name
  
  tags = {
    Name = "private-instance"
  }
}
resource "aws_key_pair" "mykeypair" {
  key_name   = "mykeypair"
  public_key = "19:80:a1:a1:a5:14:74:50:37:b5:ba:0e:c8:b7:86:3c root@10.0.2.15"
}
  

